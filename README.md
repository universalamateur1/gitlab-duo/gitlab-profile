# GitLab Duo
The suite of AI capabilities powering your workflows

* [Generally Available AI features](https://docs.gitlab.com/ee/user/ai_features.html#generally-available-ai-features)
    * [Suggested Reviewers](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/index.html#suggested-reviewers)
* [Beta AI features](https://docs.gitlab.com/ee/user/ai_features.html#beta-ai-features)
    * [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)
* [Experiment AI features](https://docs.gitlab.com/ee/user/ai_features.html#experiment-ai-features)
    * [Explain Selected Code in the Web UI](https://docs.gitlab.com/ee/user/ai_features.html#explain-selected-code-in-the-web-ui)
    * [Explain this Vulnerability in the Web UI](https://docs.gitlab.com/ee/user/ai_features.html#explain-this-vulnerability-in-the-web-ui)
    * [GitLab Duo Chat](https://docs.gitlab.com/ee/user/ai_features.html#gitlab-duo-chat)
    * [Summarize merge request changes](https://docs.gitlab.com/ee/user/ai_features.html#summarize-merge-request-changes)
    * [Summarize my merge request review](https://docs.gitlab.com/ee/user/ai_features.html#summarize-my-merge-request-review)
    * [Generate suggested tests in merge requests](https://docs.gitlab.com/ee/user/ai_features.html#generate-suggested-tests-in-merge-requests)
    * [Summarize issue discussions](https://docs.gitlab.com/ee/user/ai_features.html#summarize-issue-discussions)

## Here you have short demo Videos of the GitLab Ai Features in a developer workflow:

* [GitLab Duo -- AI-Powered DevSecOps Demo](https://www.youtube.com/watch?v=LifJdU3Qagw)
* [Walkthrough: Leveraging GitLab Vulnerability Insights + AI to Remediate a SQL-Injection](https://www.youtube.com/watch?v=EJXAIzXNAWQ)
* [GitLab Generative AI Feature Walkthrough](https://www.youtube.com/watch?v=ILJeqWoVswM)

## For completeness our Documentation around the AI Features:

* [Data Usage](https://docs.gitlab.com/ee/user/ai_features.html#data-usage)
* [Progressive enhancement ](https://docs.gitlab.com/ee/user/ai_features.html#progressive-enhancement)
* [Stability and performance ](https://docs.gitlab.com/ee/user/ai_features.html#stability-and-performance)
* [Third party services ](https://docs.gitlab.com/ee/user/ai_features.html#third-party-services)
* [Data privacy Model accuracy and quality](https://docs.gitlab.com/ee/user/ai_features.html#data-privacy)
* [GitLab Testing Agreement](https://about.gitlab.com/handbook/legal/testing-agreement/)

## AI-powered workflows
Boost efficiency and reduce cycle times with the help of AI in every phase of the software development lifecycle.
## Privacy-first, enterprise-grade
We lead with a privacy-first approach to help enterprises and regulated organizations adopt AI-powered workflows.
## Single application
A single application with built-in security to deliver more software faster, enabling executive visibility across value streams and preventing context switching.
## Empower every user at every step
From planning and code creation to testing, security, and monitoring, our AI-assisted workflows support developer, security, and ops teams.